# C'est pas fini !!!

Les premières possiblités de personnalisation sont :

- Choisir le nombre de cases vides pour les variations de caractéristiques :
    
    Utiliser `\listeenergies x`, `\listestatistiques y` et `\listecombats z`, où `x`, `y` et `z` sont le nombre de cases vides pour chaque table.
	
- Choisir s'il y a de la magie ou non :
    
    Utiliser `\listeenergies*` au lieu de `\listeenergies`, et `\listemagies*` au lieu de `\listemagies`, pour faire disparaître tout ce qui est spécifique à un adepte de magie.


Et rassurez-vous, le côté graphique (polices d'écriture, illustrations, etc.) sera **la toute dernière étape**. Donc ça sera fait, mais pas tout de suite.



# MàJ du 14/07/2017

Les listes personnalisées sont là !

```
\nouvelleliste{<nom_de_la_liste>}{<nombre_de_colonnes>} <nom_de_la_première_colonne> :: <nom_de_la_deuxième_colonne> :: <etc.>
```

J'ai dû forcer l'usage de délimiteurs pour contourner des problèmes d'expansion de macro.
C'est pas la meilleure pratique, mais ça fonctionne à merveille.
Pensez bien à ajouter `::` **après chaque colonne**.

La fonction ci-dessus crée une nouvelle macro `\<nom_de_la_liste>`, que vous pourrez utiliser ainsi :

```
\<nom_de_la_liste> <nombre_de_lignes>
```

Encore une fois, c'est loin d'être fini, mais ça commence à prendre forme.
Je ferai en sorte qu'on puisse choisir la largeur des colonnes, puis pré-remplir ces listes.
Techniquement, le plus dur est fait.



# MàJ du 14/07/2017 bis

On peut maintenant compléter directement les listes :

```
\ajoutertruc{<nom_de_la_liste>} <contenu_de_la_première_colonne> :: <contenu_de_la_deuxième_colonne> :: <etc.>
```

Le nombre total de lignes ne change pas.
Ou, pour être plus précis, le nombre de lignes vierges dans chaque liste est ajusté pour atteindre le nombre total de lignes indiqué plus haut.
(S'il y a plus d'objets dans la liste que de lignes initiales, c'est pas un problème, la liste s'agrandit.)
